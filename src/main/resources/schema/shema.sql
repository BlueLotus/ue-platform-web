CREATE TABLE project_info (
  id int(10) NOT NULL AUTO_INCREMENT,
  project_name varchar(100) NOT NULL,
  apk_name varchar(100) NOT NULL,
  apk_upload_time datetime NOT NULL,
  established tinyint(4) NOT NULL,
  analysis_state varchar(15) NOT NULL,
  start_time datetime DEFAULT NULL,
  end_time datetime DEFAULT NULL,
  uptime int(20) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE emulation_environment_info (
  id int(10) NOT NULL AUTO_INCREMENT,
  emulator_name varchar(100) NOT NULL,
  scheduled tinyint(4) NOT NULL DEFAULT '0',
  emulator_launched tinyint(4) NOT NULL,
  logcat_launched tinyint(4) NOT NULL,
  can_install_apk tinyint(4) NOT NULL,
  apk_installed tinyint(4) NOT NULL,
  analytics_project_id int(10) NOT NULL,
  apk_for_analytics varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE project_analysis_tree_result (
  id int(10) NOT NULL AUTO_INCREMENT,
  project_id int(10) NOT NULL,
  result longtext,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE apk_manifest_info (
  id int(10) NOT NULL AUTO_INCREMENT,
  project_id int(10) NOT NULL,
  package_name varchar(150) DEFAULT NULL,
  uses_permission longtext,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE system_config (
  id INT(10) NOT NULL AUTO_INCREMENT,
  path_to_apk_tool VARCHAR(250) NULL,
  path_to_android_sdk VARCHAR(250) NULL,
  path_to_taint_droid_room VARCHAR(250) NULL,
  PRIMARY KEY (id));