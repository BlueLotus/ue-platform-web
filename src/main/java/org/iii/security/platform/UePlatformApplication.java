package org.iii.security.platform;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class UePlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(UePlatformApplication.class, args);
    }
    
    @Configuration
    static class WebAppConfig extends WebMvcConfigurerAdapter {
        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        	PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
            resolver.setFallbackPageable(new PageRequest(0, 5));
            argumentResolvers.add(resolver);
        }
    }
    
}
