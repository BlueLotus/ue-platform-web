package org.iii.security.platform.web.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.iii.security.platform.core.domain.APKManifestInfo;
import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.TreeDiagramResult;
import org.iii.security.platform.core.persistence.APKManifestInfoRepository;
import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.persistence.ProjectRepository;
import org.iii.security.platform.core.persistence.TreeDiagramResultRepository;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.iii.security.platform.web.process.ActivitySnapshotCatcherProcess;
import org.iii.security.platform.web.process.MonkeyRunnerScriptPlayer;
import org.iii.security.platform.web.service.ProjectInfoService;
import org.iii.security.platform.web.util.UEPlatformWebTool;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class ProjectInfoServiceImpl implements ProjectInfoService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectInfoServiceImpl.class);
	
	private static final String DEFAULT_TREE_DIAGRAM_RESULT = "{\"name\":\"unknown\",\"level\":\"darkgray\",\"value\":10,\"children\":[],\"detailurl\":\"\"}";
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private EmulatorEnvRepository emulatorEnvRepository;
	
	@Autowired
	private TreeDiagramResultRepository treeDiagramResultRepository;
	
	@Autowired
	private APKManifestInfoRepository apkManifestInfoRepository;
	
	@Autowired
	private ProcessBuilderGenerator processBuilderGenerator;

	@Override
	public Project findById(long id) {
		return projectRepository.findOne(id);
	}
	
	@Override
	public boolean establish(Project project) {
		boolean established = false;
		List<EmulationEnv> envs = emulatorEnvRepository.findByScheduledIsFalse();
		if(envs.isEmpty()) {
			logger.debug("No available emulator environment now..., please try later.");
		} else {
			EmulationEnv emulationEnv = envs.get(0);
			emulationEnv.setScheduled(true);			
			emulationEnv.setAnalyticsProjectId(project.getId());
			emulationEnv.setApkForAnalytics(project.getApkName());
			emulatorEnvRepository.save(emulationEnv);
			project.setEstablished(true);
			project.setAnalysisState(UEPlatformWebConstant.ANALYSIS_STATE_RUNNING);
			project.setStartTime(new Date(System.currentTimeMillis()));
			projectRepository.save(project);
			TreeDiagramResult treeDiagramResult = new TreeDiagramResult();
			treeDiagramResult.setProjectId(project.getId());
			treeDiagramResult.setResult(DEFAULT_TREE_DIAGRAM_RESULT);
			treeDiagramResultRepository.save(treeDiagramResult);
			established = true;
			logger.debug("Project {} has established with apk file {} successfully.", project.getProjectName(), project.getApkName());
		}
		return established;
	}
	
	@Override
	public boolean terminate(Project project) {
		boolean terminated = false;
		List<EmulationEnv> envs = emulatorEnvRepository.findByScheduledIsTrue();
		EmulationEnv emulationEnv = envs.get(0);
		emulatorEnvRepository.save(emulationEnv.initialize());
		project.setAnalysisState(UEPlatformWebConstant.ANALYSIS_STATE_FINISHED);
		project.setEndTime(new Date(System.currentTimeMillis()));
		project.setUptime((System.currentTimeMillis() - project.getStartTime().getTime()) / 60000);
		if(projectRepository.save(project) != null) {			
			terminated = false;
			logger.debug("Project {} has been terminated successully.", project.getProjectName());
		}
		return terminated;
	}
	
	@Override
	public Project saveProject(Project project) {
		Project saved = projectRepository.save(project);
		if(saved != null) {
			if (apkManifestInfoRepository.save(UEPlatformWebTool.initAPKManifestInfo(saved.getId())) != null) {
				logger.debug("Save project {} successully.", project.getProjectName());
			} 
		}
		return saved;
	}
	
	@Override
	public boolean deleteProject(Project project) {
		boolean deleted = false;
		try {
			if(project != null ) {
				long projectId = project.getId();
				projectRepository.delete(projectId);
				List<TreeDiagramResult> diagramResults = treeDiagramResultRepository.findByProjectId(projectId);
				if(diagramResults.size() > 0) {
					TreeDiagramResult treeDiagramResult = diagramResults.get(0);
					treeDiagramResultRepository.delete(treeDiagramResult);
				}
				deleted = true;
				logger.debug("Project {} has been deleted successully.", project.getProjectName());
			}
		} catch (Exception e) {
			logger.debug("Delete project {} failed, error message: {}", project.getProjectName(), e.getMessage());
		}
		return deleted;
	}
	
	@Override
	public List<String> findUsesPermissionsByProjectId(long projectId) {
		List<String> usesPermissions = initUsesPermissions();
		if(projectRepository.findOne(projectId).isManifestExtracted()) {
			APKManifestInfo apkManifestInfo = apkManifestInfoRepository.findByProjectId(projectId);
			if (apkManifestInfo != null) {
				usesPermissions = apkManifestInfo.transformUsesPermissionsToList(apkManifestInfo.getUsesPermission());
			}
		}
		return usesPermissions;
	}
	
	@Override
	public TreeDiagramResult findTreeDiagramResultByProjectId(long projectId) {
		return treeDiagramResultRepository.findByProjectId(projectId).get(0);
	}
	
	@Override
	public Page<Project> getAllEstablishedProjectsDesc(Pageable pageable) {
		return projectRepository.findByEstablishedIsTrueAndAnalysisStateOrderByStartTimeDesc(UEPlatformWebConstant.ANALYSIS_STATE_RUNNING, pageable);
	}

	@Override
	public Page<Project> getAllUnestablishedProjectsAsc(Pageable pageable) {
		return projectRepository.findByEstablishedIsFalseAndAnalysisStateOrderByApkUploadTimeAsc(UEPlatformWebConstant.ANALYSIS_STATE_WAITING, pageable);
	}

	@Override
	public Page<Project> getAllFinishedProjectsDesc(Pageable pageable) {
		return projectRepository.findByEstablishedIsTrueAndAnalysisStateOrderByStartTimeDesc(UEPlatformWebConstant.ANALYSIS_STATE_FINISHED, pageable);
	}
	
	@Override
	public void captureCurrentActivitySnapshotByProjectId(long projectId) throws Exception {
		ActivitySnapshotCatcherProcess snapshotCatcher = new ActivitySnapshotCatcherProcess(processBuilderGenerator);
		snapshotCatcher.captureSnapshotByProjectId(projectId);
	}
	
	@Override
	public void playbackForProject(long projectId) throws Throwable {
		MonkeyRunnerScriptPlayer scriptPlayer = new MonkeyRunnerScriptPlayer(processBuilderGenerator);
		scriptPlayer.playbackForProject(projectId);
	}
	
	private List<String> initUsesPermissions() {
		List<String> usesPermissions = new ArrayList<String>();
		usesPermissions.add("Still extracting, please wait.");
		return usesPermissions;
	}

}
