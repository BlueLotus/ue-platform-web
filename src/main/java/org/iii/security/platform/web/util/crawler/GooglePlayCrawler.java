package org.iii.security.platform.web.util.crawler;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.iii.security.platform.core.domain.APKDownloadInfo;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.akdeniz.googleplaycrawler.GooglePlay.AppDetails;
import com.akdeniz.googleplaycrawler.GooglePlay.DetailsResponse;
import com.akdeniz.googleplaycrawler.GooglePlay.Offer;
import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.akdeniz.googleplaycrawler.Utils;


/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Component
public class GooglePlayCrawler {
	
	private static Logger logger = LoggerFactory.getLogger(GooglePlayCrawler.class);
	
	private static GooglePlayAPI service;

	private static HttpClient getProxiedHttpClient(String host, Integer port) throws Exception {
		HttpClient client = new DefaultHttpClient(GooglePlayAPI.getConnectionManager());
		client.getConnectionManager().getSchemeRegistry().register(Utils.getMockedScheme());
		HttpHost proxy = new HttpHost(host, port);
		client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		return client;
	}
	
	public void setup() throws Exception {
		Properties properties = new Properties();
		properties.load(new FileInputStream(UEPlatformWebConstant.GOOGLE_PLAY_CRAWLER_HOME + "login.conf"));

		String email = properties.getProperty("email");
		String password = properties.getProperty("password");

		String host = properties.getProperty("host");
		String port = properties.getProperty("port");

		service = new GooglePlayAPI(email, password);

		if (host != null && port != null) {
			service.setClient(getProxiedHttpClient(host, Integer.valueOf(port)));
		}
		logger.debug("Crawler setup complete.");
	}
	
	public void checkin() throws Exception {
		service.checkin();
		logger.debug("Crawler checkin complete.");
	}
	
	public void login() throws Exception {
		logger.debug("Allow server to catch up after checkin...");
		Thread.sleep(5000);
		logger.debug("Start login...");
		service.login();
		logger.debug("Login successfully...");
	}
	
	public void uploadDeviceConfiguration() throws Exception {
		logger.debug("Uploading device config...");
		service.uploadDeviceConfig();
		logger.debug("Uploade device config complete.");
	}
	
	public APKDownloadInfo getDownloadInfo(final String packageName) throws Exception {
		logger.debug("Preprocessing before download apk: {}.apk", packageName);
		APKDownloadInfo downloadInfo = null;
		DetailsResponse details = service.details(packageName);
		AppDetails appDetails = details.getDocV2().getDetails().getAppDetails();
		Offer offer = details.getDocV2().getOffer(0);
		
		int versionCode = appDetails.getVersionCode();
		int offerType = offer.getOfferType();
		boolean checkoutRequired = offer.getCheckoutFlowRequired();
		// paid application...ignore
		if (checkoutRequired) {
			logger.debug("This is a paid application, ignore it.");
			return null;
		} else {
			downloadInfo = new APKDownloadInfo();
			downloadInfo.setPackageName(packageName);
			downloadInfo.setVersionCode(versionCode);
			downloadInfo.setOfferType(offerType);
		}
		return downloadInfo;
	}
	
	public InputStream downloadAPKFileWithAPKDownloadInfo(final APKDownloadInfo downloadInfo) throws Exception {
		logger.debug("Start downloading...");
		String packageName = downloadInfo.getPackageName();
		InputStream inputStream = service.download(packageName, downloadInfo.getVersionCode(), downloadInfo.getOfferType());
		return inputStream;
	}

}
