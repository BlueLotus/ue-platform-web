package org.iii.security.platform.web.service.impl;

import java.util.List;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.SystemConfig;
import org.iii.security.platform.core.persistence.EmulatorEnvRepository;
import org.iii.security.platform.core.persistence.SystemConfigRepository;
import org.iii.security.platform.web.service.EmulatorEnvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class EmulatorEnvServiceImpl implements EmulatorEnvService {

	@Autowired
	private EmulatorEnvRepository emulatorEnvRepository;
	
	@Autowired
	private SystemConfigRepository systemConfigRepository;
	
	@Override
	public EmulationEnv findById(long emulatorId) {
		return emulatorEnvRepository.findOne(emulatorId);
	}
	
	@Override
	public SystemConfig findSystemConfigWithId(long sysConfigId) {
		return systemConfigRepository.findOne(sysConfigId);
	}
	
	@Override
	public void updateEnvState(EmulationEnv env) {
		emulatorEnvRepository.save(env);
	}
	
	@Override
	public boolean updateSystemConfig(SystemConfig sysConfig) {
		boolean saved = false;
		if(systemConfigRepository.save(sysConfig) != null) {
			saved = true;
		}
		return saved;
	}
	
	@Override
	public Page<EmulationEnv> findAll(Pageable pageable) {
		return emulatorEnvRepository.findAll(pageable);
	}

	@Override
	public boolean isReadyForAnalysis(Project project) {
		boolean currentProjectTargeted = false;
		boolean envCanRunAnalysis = false;
		List<EmulationEnv> targeted = emulatorEnvRepository.findByAnalyticsProjectIdAndApkForAnalytics(project.getId(), project.getApkName());
		if(targeted.size() > 0) {
			currentProjectTargeted = true;
			if(targeted.get(0).canRunAnalysis()) {
				envCanRunAnalysis = true;
			}
		}
		return (envCanRunAnalysis && currentProjectTargeted);
	}

}
