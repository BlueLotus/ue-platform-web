package org.iii.security.platform.web.pagination;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class PageItem {

	private int number;
	private boolean current;
	
	public PageItem(int number, boolean current) {
		this.number = number;
		this.current = current;
	}
	
	public int getNumber() {
		return this.number;
	}
	
	public boolean isCurrent() {
		return this.current;
	}
	
}
