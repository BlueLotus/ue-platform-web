package org.iii.security.platform.web.controller;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.SystemConfig;
import org.iii.security.platform.web.pagination.PageWrapper;
import org.iii.security.platform.web.service.EmulatorEnvService;
import org.iii.security.platform.web.util.UEPlatformWebTool;
import org.iii.security.platform.web.util.constant.UEPlatformPageResources;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR)
public class EmulatorController {
	
	private static final Logger logger = LoggerFactory.getLogger(EmulatorController.class);
	
	@Autowired
	EmulatorEnvService emulatorEnvService;
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_EMULATOR_DETAIL + "/{emulatorId}", method = RequestMethod.GET)
	public String showEmulatorDetail(@PathVariable long emulatorId, Model model) {
		model.addAttribute("emulator", emulatorEnvService.findById(emulatorId));
		model.addAttribute("sysConfig", emulatorEnvService.findSystemConfigWithId(1l));
		return UEPlatformPageResources.WEB_PAGE_FOR_EMULATOR_DETAIL;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_ALL_EMULATORS, method = RequestMethod.GET)
	public String showAllEmulators(Model model, Pageable pageable) {
		PageWrapper<EmulationEnv> page = new PageWrapper<>(emulatorEnvService.findAll(pageable), UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR, UEPlatformWebRouting.URL_FOR_ALL_EMULATORS);
		model.addAttribute("page", page);
		model.addAttribute("emulators", page.getContent());
		return UEPlatformPageResources.WEB_PAGE_FOR_ALL_EMULATOR_LIST;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_CREATE_EMULATOR, method = RequestMethod.GET)
	public String createEmulator() {
		return "";
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_CREATE_SYSTEM_CONFIG, method = RequestMethod.GET)
	public String createSystemConfig() {
		return "";
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_INITIALIZE_EMULATOR_STATE + "/{emulatorId}", method = RequestMethod.GET)
	@ResponseBody
	public String initializeEmulatorState(@PathVariable long emulatorId) {
		EmulationEnv env;
		String result = "Initialize emulator state failed, please check the root cause in log file.";
		try {
			env = emulatorEnvService.findById(emulatorId);
			emulatorEnvService.updateEnvState(env.initialize());
			result = "Initialize emulator state successfully.";
		} catch (RuntimeException e) {
			logger.debug("Something wrong when initializing emulator, the error message is: {}", e.getMessage());
		}
		return result;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_EMULATOR_MANAGEMENT, method = RequestMethod.GET)
	public String manageEmulators(Model model, Pageable pageable) {
		PageWrapper<EmulationEnv> page = new PageWrapper<>(emulatorEnvService.findAll(pageable), UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR, UEPlatformWebRouting.URL_FOR_EMULATOR_MANAGEMENT);
		model.addAttribute("page", page);
		model.addAttribute("emulators", page.getContent());
		return UEPlatformPageResources.WEB_PAGE_FOR_EMULATOR_MANAGEMENT;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_EMULATOR_CONFIG_SETTING + "/{emulatorId}", method = RequestMethod.GET)
	public String provideSystemConfigForEmulator(@PathVariable long emulatorId, Model model) {
		model.addAttribute("emulatorId", emulatorId);
		return UEPlatformPageResources.WEB_PAGE_FOR_EMULATOR_CONFIG_SETTING;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_EMULATOR_CONFIG_SETTING + "/{emulatorId}", method = RequestMethod.POST)
	public String modifySystemConfigForEmulator(@PathVariable String emulatorId, 
			@RequestParam("pathToApktool") String pathToApktool, 
			@RequestParam("pathToAndroidSdk") String pathToAndroidSdk, 
			@RequestParam("pathToTaintDroidRoom") String pathToTaintDroidRoom, Model model) {
		boolean configResult = false;
		SystemConfig sysConfig = emulatorEnvService.findSystemConfigWithId(1l);
		sysConfig.setPathToApktool(pathToApktool);
		sysConfig.setPathToAndroidSdk(pathToAndroidSdk);
		sysConfig.setPathToTaintDroidRoom(pathToTaintDroidRoom);
		if(emulatorEnvService.updateSystemConfig(sysConfig)) {
			configResult = true;
			model.addAttribute("msg", UEPlatformWebTool.obtainOperationDoneMessage(UEPlatformWebConstant.OPERATION_TYPE_FOR_CONFIG_SYSTEM_SUCCESSFULLY));
			logger.debug("Update system parameters for emulator {} successfully.", emulatorId);
		} else {
			model.addAttribute("msg", UEPlatformWebTool.obtainOperationDoneMessage(UEPlatformWebConstant.OPERATION_TYPE_FOR_CONFIG_SYSTEM_FAILED));
		}
		model.addAttribute("opSuccess", configResult);
		return UEPlatformPageResources.WEB_PAGE_FOR_OPERATION_DONE;
	}
	
}
