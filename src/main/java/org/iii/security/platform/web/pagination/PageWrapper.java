package org.iii.security.platform.web.pagination;

import java.util.ArrayList;
import java.util.List;

import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.springframework.data.domain.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class PageWrapper<T> {
	
	public static final int MAX_PAGES_FOR_DISPLAY = 5;
	private Page<T> page;
	private List<PageItem> items;
	private int currentNumber;
	private String contextUrl;
	private String url;
	
	public PageWrapper(Page<T> page, String contextUrl, String url) {
		this.page = page;
		this.contextUrl = contextUrl;
		this.url = url;
		items = new ArrayList<PageItem>();
		
		currentNumber = page.getNumber() + 1;
		
		int start;
		int size = 0;
		
		if(page.getTotalPages() <= MAX_PAGES_FOR_DISPLAY) {
			start = 1;
			size = page.getTotalPages();
		} else {
			if (currentNumber <= MAX_PAGES_FOR_DISPLAY - MAX_PAGES_FOR_DISPLAY/2) {
				start = 1;
				size = MAX_PAGES_FOR_DISPLAY;
			} else if(currentNumber >= page.getTotalPages() - MAX_PAGES_FOR_DISPLAY/2) {
				start = page.getTotalPages() - MAX_PAGES_FOR_DISPLAY + 1;
				size = MAX_PAGES_FOR_DISPLAY;
			} else {
				start = currentNumber - MAX_PAGES_FOR_DISPLAY/2;
				size = MAX_PAGES_FOR_DISPLAY;
			}
		}
		for(int i = 0; i < size; i++) {
			items.add(new PageItem(start + i, (start + i) == currentNumber));
		}
	}
	
	public String getUrl() {
		return contextUrl + url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getEstablishUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_PROJECT_ESTABLISH_CONFIRM;
	}
	
	public String getTreeViewUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_SHOW_TREE_DIAGRAM;
	}
	
	public String getTerminateUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_PROJECT_TERMINATE_OPERATION;
	}
	
	public String getDeleteUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_PROJECT_DELETE_OPERATION;
	}
	
	public String getSystemConfigUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR + UEPlatformWebRouting.URL_FOR_EMULATOR_CONFIG_SETTING;
	}
	
	public String getEmulatorDetailUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR + UEPlatformWebRouting.URL_FOR_EMULATOR_DETAIL;
	}
	
	public String getEmulatorInitUrl() {
		return UEPlatformWebRouting.ROUTING_ROOT_FOR_EMULATOR + UEPlatformWebRouting.URL_FOR_INITIALIZE_EMULATOR_STATE;
	}
	
	public List<PageItem> getItems() {
		return items;
	}
	
	public int getNumber() {
		return currentNumber;
	}
	
	public List<T> getContent() {
		return page.getContent();
	}
	
	public int getSize() {
		return page.getSize();
	}
	
	public int getTotalPages() {
		return page.getTotalPages();
	}
	
	public boolean isFirstPage() {
		return page.isFirst();
	}
	
	public boolean isLastPage() {
		return page.isLast();
	}
	
	public boolean isHasPreviousPage() {
		return page.hasPrevious();
	}
	
	public boolean isHasNextPage() {
		return page.hasNext();
	}

}
