package org.iii.security.platform.web.process;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class MonkeyRunnerScriptPlayer {
	
	private static final Logger logger = LoggerFactory.getLogger(MonkeyRunnerScriptPlayer.class);
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	int attempt;
	boolean playbackSuccessfully = false;

	public MonkeyRunnerScriptPlayer(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
		this.attempt = 0;
	}
	
	public void playbackForProject(long projectId) throws Throwable {
		BufferedReader processReader = null;
		BufferedReader errorReader = null;
		do {
			try {
				processBuilderGenerator.setProjectId(projectId);
				ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_MONKEY_RUNNER_SCRIPT_PLAYER_PROCESS);
				Process process = processBuilder.start();
				
				processReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
	            
				logger.debug("Playback start...");
				String processMessage;
				logger.debug("Standard error: ");
				while ((processMessage = errorReader.readLine()) != null) {
					logger.debug(processMessage);
					if(processMessage.contains("No such file or directory")) {
						throw new RuntimeException(UEPlatformWebConstant.MSG_FOR_RECORDED_SCRIPT_NOT_FOUND);
					} else if(processMessage.contains("Error sending touch event")) {
						throw new RuntimeException(UEPlatformWebConstant.MSG_FOR_PLAYBACK_FAILED);
					} else if(processMessage.contains("] at")) {
						throw new RuntimeException(UEPlatformWebConstant.MSG_FOR_EXCEPTION_OCCURRED);
	                }
	            }
				logger.debug("Standard output: ");
				while ((processMessage = processReader.readLine()) != null) {
					logger.debug(processMessage);
					if(processMessage.contains("Can't open specified script file")) {
						throw new RuntimeException(UEPlatformWebConstant.MSG_FOR_PLAYBACK_SCRIPT_NOT_FOUND);
						}
					}
				playbackSuccessfully = true;
				logger.debug("Playback successfully...");
			} catch (Exception e) {
				if(rescuable(e)) {
					if(attempt < 3) {
						logger.debug("Playback failed for {} time(s), auto retry again.", ++attempt);
					} else {
						throw e;
					}
				} else {
					throw e;
				}
			} finally {
				if(processReader != null ) processReader.close();
				if(errorReader != null) errorReader.close();
			}
		} while (isRecurrent());
	}
	
	private boolean rescuable(Exception e) {
		return e.getMessage().contains(UEPlatformWebConstant.MSG_FOR_PLAYBACK_FAILED);
	}
	private boolean isRecurrent() {
		return !playbackSuccessfully && attempt < 3;
	}

}
