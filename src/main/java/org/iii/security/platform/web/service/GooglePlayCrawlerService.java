package org.iii.security.platform.web.service;

import java.io.InputStream;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface GooglePlayCrawlerService {
	
	public InputStream crawlAPKWithPackageName(String packageName) throws Exception;

}
