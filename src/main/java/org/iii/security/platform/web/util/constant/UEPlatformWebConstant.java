package org.iii.security.platform.web.util.constant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformWebConstant {
	
	public static final int MAX_EXECUTABLE_TAINTDROID_ENV = 1;

	public static final String ANALYSIS_STATE_WAITING = "waiting";
	public static final String ANALYSIS_STATE_RUNNING = "running";
	public static final String ANALYSIS_STATE_FINISHED = "finished";
	
	public static final String OPERATION_TYPE_FOR_ESTABLISH_PROJECT_SUCCESSFULLY = "establishProjectSuccessfully";
	public static final String OPERATION_TYPE_FOR_ESTABLISH_PROJECT_FAILED = "establishProjectFailed";
	public static final String OPERATION_TYPE_FOR_TERMINATE_PROJECT_SUCCESSFULLY = "terminateProjectSuccessfully";
	public static final String OPERATION_TYPE_FOR_TERMINATE_PROJECT_FAILED = "terminateProjectFailed";
	public static final String OPERATION_TYPE_FOR_DELETE_PROJECT_SUCCESSFULLY = "deleteProjectSuccessfully";
	public static final String OPERATION_TYPE_FOR_DELETE_PROJECT_FAILED = "deleteProjectFailed";
	public static final String OPERATION_TYPE_FOR_CONFIG_SYSTEM_SUCCESSFULLY = "configSystemSuccessfully";
	public static final String OPERATION_TYPE_FOR_CONFIG_SYSTEM_FAILED = "configSystemFailed";
	
	public static final String GOOGLE_PLAY_CRAWLER_HOME = "C:\\UE_analysis\\google-play-crawler\\";
	
	public static final String MSG_FOR_PLAYBACK_SCRIPT_NOT_FOUND = "PlaybackScriptNotFoundException";
	public static final String MSG_FOR_PLAYBACK_FAILED = "PlaybackFailedException";
	public static final String MSG_FOR_RECORDED_SCRIPT_NOT_FOUND = "RecordedScriptNotFoundException";
	public static final String MSG_FOR_EXCEPTION_OCCURRED = "UnknownException";
	
}
