package org.iii.security.platform.web.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.process.ProcessBuilderGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ActivitySnapshotCatcherProcess {
	
	private static final Logger logger = LoggerFactory.getLogger(ActivitySnapshotCatcherProcess.class);
	
	private ProcessBuilderGenerator processBuilderGenerator;
	
	private String snapshotName;
	
	public ActivitySnapshotCatcherProcess(ProcessBuilderGenerator processBuilderGenerator) {
		this.processBuilderGenerator = processBuilderGenerator;
	}
	
	public void captureSnapshotByProjectId(long projectId) throws Exception {
		try {
			processBuilderGenerator.setProjectId(projectId);
			executeProcessWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_SYSTEM_DUMP_PROCESS);
			executeProcessWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_ACTIVITY_SNAPSHOT_CATCHER_PROCESS);
			executeProcessWithSpecifiedScenario(EmulatorJobConstant.OBTAIN_PULL_SNAPSHOT_PROCESS);
		} catch (IOException e) {
			throw new Exception("Capture snapshot failed, the error message is: " + e.getMessage());
		}
	}
	
	private void executeProcessWithSpecifiedScenario(int scenario) throws Exception {
		ProcessBuilder processBuilder = processBuilderGenerator.generateProcessBuilderWithSpecifiedScenario(scenario);
		Process process = processBuilder.start();
		BufferedReader processReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String processMessage;
		if(scenario == EmulatorJobConstant.OBTAIN_SYSTEM_DUMP_PROCESS) {
			while((processMessage = processReader.readLine()) != null) {
				if(processMessage.contains("mFocusedActivity")){
					snapshotName = filterOutActivityName(processMessage).concat(".jpg");
					processBuilderGenerator.setSnapshotName(snapshotName);
					logger.debug("Capture activity snapshot: {}", snapshotName);
				}
			}
		} else if(scenario == EmulatorJobConstant.OBTAIN_ACTIVITY_SNAPSHOT_CATCHER_PROCESS){
			while((processMessage = processReader.readLine()) != null) {
				if(processMessage.contains("Read-only file system")) {
					processReader.close();
					throw new Exception("Read-only error.");
				}
			}
		} else {
			while((processMessage = processReader.readLine()) != null) {
			}
		}
		processReader.close();
	}
	
	private String filterOutActivityName(String processMessage) {
		String[] msgs = processMessage.split("/");
		return msgs[1].substring(0, msgs[1].length() - 1);
	}
}
