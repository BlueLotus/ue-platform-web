package org.iii.security.platform.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.iii.security.platform.core.util.constant.UEPlatformConstant;
import org.iii.security.platform.web.service.ProjectInfoService;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.ROUTING_ROOT_FOR_ACTIVITY_SNAPSHOT)
public class ActivitySnapshotController {
	
	private static final Logger logger = LoggerFactory.getLogger(ActivitySnapshotController.class);
	
	@Autowired
	private ProjectInfoService projectInfoService;
	
	@RequestMapping(value = "/snapshot/projectId/{projectId}/activityName/{activityName}", method = RequestMethod.GET, produces = "image/jpg")
	@ResponseBody
	public byte[] getActivitySnapshotWithProjectId(@PathVariable String projectId, @PathVariable String activityName) {
		String pathToActivitySnapshot = UEPlatformConstant.ACTIVITY_SNAPSHOT_LOCATION + projectId + "\\" + activityName + ".jpg";
		byte[] result = null;
		BufferedImage image;
		ByteArrayOutputStream byteArrayOutputStream;
		try {
			image = ImageIO.read(new File(pathToActivitySnapshot));
			byteArrayOutputStream = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", byteArrayOutputStream);
			result = byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			if(e.getClass() == IIOException.class)
				logger.debug("Can't read the image file, please check the image exist or not.");
			else
				logger.debug("Something wrong when reading image, the error message is: {}", e.getMessage());
			try {
				image = ImageIO.read(new File(UEPlatformConstant.ACTIVITY_SNAPSHOT_NOT_FOUND));
				byteArrayOutputStream = new ByteArrayOutputStream();
				ImageIO.write(image, "jpg", byteArrayOutputStream);
				result = byteArrayOutputStream.toByteArray();
			} catch (IOException innerException) {
				logger.debug("Something wrong when reading default image, the error message is: {}", innerException.getMessage());
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/snapshot/capturedById/{projectId}", method = RequestMethod.GET)
	@ResponseBody
	public String captureCurrentActivitySnapshotByProjectId(@PathVariable String projectId, HttpServletResponse response) {
		String result;
		logger.debug("Capturing current activity snapshot...");
		try {
			projectInfoService.captureCurrentActivitySnapshotByProjectId(Long.valueOf(projectId));
			result = "Captured successfully.";
		} catch (Exception e) {
			logger.debug("Something wrong when capturing activity snapshot, the error message is: {}", e.getMessage());
			result = String.format("Captured failed, the error message is: %s.", e.getMessage());
		}
		return result;
	}

}
