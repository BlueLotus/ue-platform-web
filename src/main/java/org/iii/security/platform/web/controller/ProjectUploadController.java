package org.iii.security.platform.web.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.iii.security.platform.core.util.constant.UEPlatformConstant;
import org.iii.security.platform.web.service.ProjectInfoService;
import org.iii.security.platform.web.util.UEPlatformWebTool;
import org.iii.security.platform.web.util.constant.UEPlatformPageResources;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT)
public class ProjectUploadController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectUploadController.class);
	
	@Autowired
	private ProjectInfoService projectInfoService;
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_UPLOAD_MANUAL_TESTING_OPERATION, method = RequestMethod.GET)
	public String provideUploadInfoForManualTesting(Model model) {
		model.addAttribute("msg", "Please enter the following information for building a manual analysis project:");
		return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_UPLOAD_MANUAL_TESTING_OPERATION, method = RequestMethod.POST)
	public String handleFileUploadForManualTesting(@RequestParam("projectName") String projectName, 
			@RequestParam("apkFile") MultipartFile apkFile, Model model) {
		String apkName = apkFile.getOriginalFilename();
		if(!apkFile.isEmpty() && !projectName.isEmpty()) {
			try {
				byte[] apkFileDataByte = apkFile.getBytes();
				BufferedOutputStream streamForApkFile = new BufferedOutputStream(new FileOutputStream(new File(UEPlatformConstant.APK_FILE_LOCATION + apkName)));
				streamForApkFile.write(apkFileDataByte);
				streamForApkFile.close();
				projectInfoService.saveProject(UEPlatformWebTool.initProjectObject(projectName, apkName));
				model.addAttribute("msg", "Project " + projectName + " created successfully with " + apkName + "!");
				return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK;
			} catch (Exception e) {
				logger.debug("Project {} can not be created with apk file: {}.", projectName, apkFile);
				logger.debug("There is something wrong when uploading the apk file, error message: {}", e.getMessage());
				model.addAttribute("msg", "You failed to create " + projectName + " project, please call the system admin to check the problem.");
				return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK;
			}
		} else {
			model.addAttribute("msg", "You failed to create the project because the project name/file was empty.");
			return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK;
		}
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_UPLOAD_AUTO_TESTING_OPERATION, method = RequestMethod.GET)
	public String provideUploadInfoForAutoTesting(Model model) {
		model.addAttribute("msg", "Please enter the following information for building an auto analysis project:");
		return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK_WITH_SCRIPT;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_UPLOAD_AUTO_TESTING_OPERATION, method = RequestMethod.POST)
	public String handleFileUploadForAutoTesting(@RequestParam("projectName") String projectName, 
			@RequestParam("apkFile") MultipartFile apkFile, @RequestParam("scriptFile") MultipartFile scriptFile, Model model) {
		String apkName = apkFile.getOriginalFilename();
		long projectId = projectInfoService.saveProject(UEPlatformWebTool.initProjectObject(projectName, apkName)).getId();
		if(!apkFile.isEmpty() && !scriptFile.isEmpty() && !projectName.isEmpty()) {
			try {
				byte[] apkFileDataByte = apkFile.getBytes();
				BufferedOutputStream streamForApkFile = new BufferedOutputStream(new FileOutputStream(new File(UEPlatformConstant.APK_FILE_LOCATION + apkName)));
				streamForApkFile.write(apkFileDataByte);
				streamForApkFile.close();
				
				String scriptName = UEPlatformConstant.MONKEY_SCRIPT_FILE_LOCATION + projectId + EmulatorJobConstant.MONKEY_RUNNER_SCRIPT_FOR_PLAY_BACK;
				String scriptDirPath = scriptName.substring(0, scriptName.lastIndexOf("\\"));
				File scriptDir = new File(scriptDirPath); 
				if(!scriptDir.exists()) {
					if(scriptDir.mkdir()) {
						byte[] scriptDataByte = scriptFile.getBytes();
						BufferedOutputStream streamForScript = new BufferedOutputStream(new FileOutputStream(new File(scriptName)));
						streamForScript.write(scriptDataByte);
						streamForScript.close();
					} else {
						throw new RuntimeException("Can not create dir for script...");
					}
				} else {
					throw new RuntimeException("Script already existed...");
				}
				model.addAttribute("msg", "Project " + projectName + " created successfully with " + apkName + "!");
				return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK_WITH_SCRIPT;
			} catch (Exception e) {
				rollbackEstablishedProject(projectId);
				logger.debug("Project {} can not be created with apk file: {}.", projectName, apkFile);
				logger.debug("There is something wrong when uploading the apk file, error message: {}", e.getMessage());
				model.addAttribute("msg", "You failed to create " + projectName + " project, please call the system admin to check the problem.");
				return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK_WITH_SCRIPT;
			}
		} else {
			model.addAttribute("msg", "You failed to create the project because the project name/file/script was empty.");
			return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK_WITH_SCRIPT;
		}
	}
	
	private void rollbackEstablishedProject(long projectId) {
		Project deleted = projectInfoService.findById(projectId);
		if(deleted != null) {
			projectInfoService.deleteProject(deleted);
		}
	}

}
