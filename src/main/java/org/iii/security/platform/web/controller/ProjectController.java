package org.iii.security.platform.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.TreeDiagramResult;
import org.iii.security.platform.web.pagination.PageWrapper;
import org.iii.security.platform.web.service.EmulatorEnvService;
import org.iii.security.platform.web.service.ProjectInfoService;
import org.iii.security.platform.web.util.UEPlatformWebTool;
import org.iii.security.platform.web.util.constant.UEPlatformPageResources;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT)
public class ProjectController {
	
	@Autowired
	private ProjectInfoService projectInfoService;
	
	@Autowired
	private EmulatorEnvService emulatorEnvService;
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_ESTABLISHED_PROJECTS, method = RequestMethod.GET)
	public String showEstablishedProject(Model model, Pageable pageable) {
		PageWrapper<Project> page = new PageWrapper<Project>(projectInfoService.getAllEstablishedProjectsDesc(pageable), UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT, UEPlatformWebRouting.URL_FOR_ESTABLISHED_PROJECTS);
		model.addAttribute("page", page);
		model.addAttribute("projects", UEPlatformWebTool.countUptimeForProjects(page));
		return UEPlatformPageResources.WEB_PAGE_FOR_ESTABLISHED_PROJECTS_LIST;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_UNESTABLISHED_PROJECTS, method = RequestMethod.GET)
	public String showUnestablishedProject(Model model, Pageable pageable) {
		PageWrapper<Project> page = new PageWrapper<Project>(projectInfoService.getAllUnestablishedProjectsAsc(pageable), UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT, UEPlatformWebRouting.URL_FOR_UNESTABLISHED_PROJECTS);
		model.addAttribute("page", page);
		model.addAttribute("projects", page.getContent());
		return UEPlatformPageResources.WEB_PAGE_FOR_UNESTABLISHED_PROJECTS_LIST;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_FINISHED_PROJECTS, method = RequestMethod.GET)
	public String showFinishedProject(Model model, Pageable pageable) {
		PageWrapper<Project> page = new PageWrapper<Project>(projectInfoService.getAllFinishedProjectsDesc(pageable), UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT, UEPlatformWebRouting.URL_FOR_FINISHED_PROJECTS);
		model.addAttribute("page", page);
		model.addAttribute("projects", page.getContent());
		return UEPlatformPageResources.WEB_PAGE_FOR_FINISHED_PROJECTS_LIST;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_ESTABLISH_CONFIRM + "/{id}", method = RequestMethod.GET)
	public String confirmEstablishProjectPage(@PathVariable long id, Model model) {
		Project unestablishedProject = projectInfoService.findById(id);
		model.addAttribute("id", id);
		model.addAttribute("unestablishedProject", unestablishedProject);
		model.addAttribute("establishPage", UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_PROJECT_ESTABLISH_OPERATION);
		return UEPlatformPageResources.WEB_PAGE_FOR_OPERATIONS_ESTABLISH_PROJECT;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_ESTABLISH_OPERATION + "/{id}", method = RequestMethod.GET)
	public String establishProjectWithId(@PathVariable long id, Model model) {
		String opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_ESTABLISH_PROJECT_FAILED;
		Project unestablishedProject = projectInfoService.findById(id);
		boolean established = projectInfoService.establish(unestablishedProject);
		if(established) {
			opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_ESTABLISH_PROJECT_SUCCESSFULLY;
		}
		model.addAttribute("opSuccess", established);
		return "forward:" + UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_OPERATION_DONE + "/" + opType ;
	}

	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_TERMINATE_OPERATION + "/{id}", method = RequestMethod.GET)
	public String terminateProjectWithId(@PathVariable long id, Model model) {
		String opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_TERMINATE_PROJECT_FAILED;
		Project project = projectInfoService.findById(id);
		boolean terminated = projectInfoService.terminate(project);
		if(terminated) {
			opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_TERMINATE_PROJECT_SUCCESSFULLY;
		}
		model.addAttribute("opSuccess", terminated);
		return "forward:" + UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_OPERATION_DONE + "/" + opType ;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_PROJECT_DELETE_OPERATION + "/{id}", method = RequestMethod.GET)
	public String deleteProjectWithId(@PathVariable long id, Model model) {
		String opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_DELETE_PROJECT_FAILED;
		Project project = projectInfoService.findById(id);
		boolean deleted = projectInfoService.deleteProject(project);
		if(deleted) {
			opType = UEPlatformWebConstant.OPERATION_TYPE_FOR_DELETE_PROJECT_SUCCESSFULLY;
		}
		model.addAttribute("opSuccess", deleted);
		return "forward:" + UEPlatformWebRouting.ROUTING_ROOT_FOR_PROJECT + UEPlatformWebRouting.URL_FOR_OPERATION_DONE + "/" + opType ;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_OPERATION_DONE + "/{operationType}", method = RequestMethod.GET)
	public String operationDone(@PathVariable String operationType, Model model) {
		model.addAttribute("msg", UEPlatformWebTool.obtainOperationDoneMessage(operationType));
		return UEPlatformPageResources.WEB_PAGE_FOR_OPERATION_DONE;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_SHOW_TREE_DIAGRAM + "/{id}", method = RequestMethod.GET)
	public String showTreeDiagram(@PathVariable long id, Model model) {
		Project project = projectInfoService.findById(id);
		boolean canCaptureSnapshot = false;
		model.addAttribute("project", project);
		if(!project.getAnalysisState().contentEquals(UEPlatformWebConstant.ANALYSIS_STATE_FINISHED) && project.getUptime() < 1) {
			model.addAttribute("temporaryUptime", UEPlatformWebTool.countUptimeForProject(project));
		}
		if(emulatorEnvService.isReadyForAnalysis(project)) {
			canCaptureSnapshot = true;
		}
		model.addAttribute("canCaptureSnapshot", canCaptureSnapshot);
		List<String> permissions = projectInfoService.findUsesPermissionsByProjectId(id);
		model.addAttribute("permissions", permissions);
		TreeDiagramResult treeDiagramResult = projectInfoService.findTreeDiagramResultByProjectId(id);
		String result = treeDiagramResult.getResult();
		model.addAttribute("result", result);
		return UEPlatformPageResources.WEB_PAGE_FOR_TREE_DIAGRAM_RESULT;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_RUN_AUTO_TESTING + "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String playbackMonkeyRunnerScript(@PathVariable long id, HttpServletResponse response) {
		String responseMsg = null;
		try {
			projectInfoService.playbackForProject(id);
			responseMsg = "Playback successfully.";
		} catch (Throwable throwable) {
			responseMsg = UEPlatformWebTool.transformThrowableToReadableMessage(throwable);
		}
		return responseMsg;
	}

}
