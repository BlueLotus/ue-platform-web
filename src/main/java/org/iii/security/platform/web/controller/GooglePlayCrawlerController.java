package org.iii.security.platform.web.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.iii.security.platform.web.service.GooglePlayCrawlerService;
import org.iii.security.platform.web.util.constant.UEPlatformPageResources;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.ROUTING_ROOT_FOR_GOOGLE_PLAY_CRAWLER)
public class GooglePlayCrawlerController {
	
	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerController.class);
	
	@Autowired
	private GooglePlayCrawlerService googlePlayCrawlerService;
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_CRAWLER_TASK, method = RequestMethod.GET)
	public String provideCrawlerInfo(Model model) {
		model.addAttribute("msg", "Please enter the following information for crawl the APK you need:");
		return UEPlatformPageResources.WEB_PAGE_FOR_GOOGLE_PLAY_CRAWLER_CRAWL_TASK;
	}
	
	@RequestMapping(value = UEPlatformWebRouting.URL_FOR_CRAWLER_TASK, method = RequestMethod.POST)
	public void crawlSpecificAPKFile(@RequestParam("packageName") String packageName, Model model, HttpServletResponse response) throws Exception {
		String apkFileName = packageName + ".apk";
		String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", apkFileName);
        
        InputStream inputStream = null;
        ByteArrayOutputStream streamForCountSize = new ByteArrayOutputStream();
        OutputStream outStream = response.getOutputStream();
        
        try {
        	inputStream = googlePlayCrawlerService.crawlAPKWithPackageName(packageName);
        	response.setHeader(headerKey, headerValue);
            response.setContentType("application/octet-stream");
            
        	byte[] buffer = new byte[1024];
            int bytesRead = -1;
    		
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
                streamForCountSize.write(buffer, 0, bytesRead);
            }
            
            response.setContentLength(streamForCountSize.size());
            logger.debug("Download apk successfully.");
		} catch (Exception e) {
			throw new Exception("Download APK failed exception.");
		} finally {
			inputStream.close();
			outStream.close();
		} 
	}
	
}
