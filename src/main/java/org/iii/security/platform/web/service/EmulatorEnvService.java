package org.iii.security.platform.web.service;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.SystemConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface EmulatorEnvService {
	
	EmulationEnv findById(long emulatorId);
	
	SystemConfig findSystemConfigWithId(long sysConfigId);
	
	void updateEnvState(EmulationEnv env);
	
	boolean updateSystemConfig(SystemConfig sysConfig);
	
	Page<EmulationEnv> findAll(Pageable pageable);
	
	boolean isReadyForAnalysis(Project project);

}
