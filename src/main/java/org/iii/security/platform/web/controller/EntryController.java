package org.iii.security.platform.web.controller;

import org.iii.security.platform.web.util.constant.UEPlatformPageResources;
import org.iii.security.platform.web.util.constant.UEPlatformWebRouting;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = UEPlatformWebRouting.WEB_APP_CONTEXT_ROOT)
public class EntryController {
	
	@RequestMapping(value = "/")
	public String webRoot(){
		return "forward:" + UEPlatformWebRouting.URL_FOR_HOME_PAGE;
	}
	
	@RequestMapping(value = "/home")
	public String home() {
		return UEPlatformPageResources.WEB_PAGE_FOR_HOME;
	}
	
}
