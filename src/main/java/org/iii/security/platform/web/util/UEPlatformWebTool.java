package org.iii.security.platform.web.util;

import java.util.Date;
import java.util.List;

import org.iii.security.platform.core.domain.APKManifestInfo;
import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.web.pagination.PageWrapper;
import org.iii.security.platform.web.util.constant.UEPlatformWebConstant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformWebTool {
	
	public static Project initProjectObject(String projectName, String apkName) {
		Project project = new Project();
		project.setProjectName(projectName);
		project.setApkName(apkName);
		project.setApkUploadTime(new Date(System.currentTimeMillis()));
		project.setManifestExtracted(false);
		project.setEstablished(false);
		project.setAnalysisState(UEPlatformWebConstant.ANALYSIS_STATE_WAITING);
		return project;
	}
	
	public static APKManifestInfo initAPKManifestInfo(long projectId) {
		APKManifestInfo apkManifestInfo = new APKManifestInfo();
		apkManifestInfo.setProjectId(projectId);
		apkManifestInfo.setPackageName("");
		apkManifestInfo.setUsesPermission("");
		return apkManifestInfo;
	}
	
	public static long countUptimeForProject(Project project) {
		return (System.currentTimeMillis() - project.getStartTime().getTime()) / 60000;
	}
	
	public static List<Project> countUptimeForProjects(PageWrapper<Project> page) {
		List<Project> projects = page.getContent();
		for (Project project : projects) {
			long uptime = countUptimeForProject(project);
			project.setUptime(uptime);
		}
		return projects;
	}
	
	public static String obtainOperationDoneMessage(String operationType) {
		String msg = null;
		switch (operationType) {
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_ESTABLISH_PROJECT_SUCCESSFULLY:
			msg = "Your project has already established, please go to the established list and check it.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_ESTABLISH_PROJECT_FAILED:
			msg = "No available emulator environment now..., please try later.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_TERMINATE_PROJECT_SUCCESSFULLY:
			msg = "You've terminated the project successfully.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_TERMINATE_PROJECT_FAILED:
			msg = "Terminate project failed, please ask the admin for checking problem.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_DELETE_PROJECT_SUCCESSFULLY:
			msg = "You've deleted the project successfully.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_DELETE_PROJECT_FAILED:
			msg = "Delete project failed, please check the project exist or not.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_CONFIG_SYSTEM_SUCCESSFULLY:
			msg = "Config system parameters successfully.";
			break;
		case UEPlatformWebConstant.OPERATION_TYPE_FOR_CONFIG_SYSTEM_FAILED:
			msg = "Config system parameters failed, please call the system admin to check the problem.";
			break;
		default:
			msg = "Oops!!! You did some wrong operations, please check it again!";
			break;
		}
		return msg;
	}
	
	public static String transformThrowableToReadableMessage(Throwable error) {
		String msg = null;
		switch (error.getMessage()) {
		case UEPlatformWebConstant.MSG_FOR_PLAYBACK_SCRIPT_NOT_FOUND:
			msg = "Test failed: Playback script not found.";
			break;
		case UEPlatformWebConstant.MSG_FOR_RECORDED_SCRIPT_NOT_FOUND:
			msg = "Test failed: Recorded script not found.";
			break;
		case UEPlatformWebConstant.MSG_FOR_PLAYBACK_FAILED:
			msg = "Test failed: Playback failed.";
			break;
		case UEPlatformWebConstant.MSG_FOR_EXCEPTION_OCCURRED:
			msg = "Test failed: Unknown exception happened.";
			break;
		default:
			msg = "Something unexpected situation happened, please call the system admin.";
			break;
		}
		return msg;
	}

}
