package org.iii.security.platform.web.util.constant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformPageResources {
	
	public static final String THYMELEAF_TEMPLATE_HOME = "web/";
	
	public static final String WEB_PAGE_DIR_FOR_EMULATOR_ROOT = THYMELEAF_TEMPLATE_HOME + "emulator/";
	public static final String WEB_PAGE_DIR_FOR_PROJECT_ROOT = THYMELEAF_TEMPLATE_HOME + "project/";
	public static final String WEB_PAGE_DIR_FOR_GOOGLE_PLAY_CRAWLER_ROOT = THYMELEAF_TEMPLATE_HOME + "crawler/";
	
	public static final String WEB_PAGE_DIR_FOR_EMULATOR_INFO = WEB_PAGE_DIR_FOR_EMULATOR_ROOT + "info/";
	public static final String WEB_PAGE_DIR_FOR_EMULATOR_LISTS = WEB_PAGE_DIR_FOR_EMULATOR_ROOT + "lists/";
	public static final String WEB_PAGE_DIR_FOR_EMULATOR_MANAGEMENT = WEB_PAGE_DIR_FOR_EMULATOR_ROOT + "management/";
	public static final String WEB_PAGE_DIR_FOR_EMULATOR_OPERATIONS = WEB_PAGE_DIR_FOR_EMULATOR_ROOT + "operations/";
	
	public static final String WEB_PAGE_DIR_FOR_INFO = WEB_PAGE_DIR_FOR_PROJECT_ROOT + "info/";
	public static final String WEB_PAGE_DIR_FOR_PROJECT_LISTS = WEB_PAGE_DIR_FOR_PROJECT_ROOT + "lists/";
	public static final String WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS = WEB_PAGE_DIR_FOR_PROJECT_ROOT + "operations/";
	
	public static final String WEB_PAGE_FOR_HOME = THYMELEAF_TEMPLATE_HOME + "home";
	
	public static final String WEB_PAGE_FOR_EMULATOR_DETAIL = WEB_PAGE_DIR_FOR_EMULATOR_INFO + "emulator_detail";
	public static final String WEB_PAGE_FOR_ALL_EMULATOR_LIST = WEB_PAGE_DIR_FOR_EMULATOR_LISTS + "all_emulators";
	public static final String WEB_PAGE_FOR_EMULATOR_MANAGEMENT = WEB_PAGE_DIR_FOR_EMULATOR_MANAGEMENT + "emulator_management";
	public static final String WEB_PAGE_FOR_EMULATOR_CONFIG_SETTING = WEB_PAGE_DIR_FOR_EMULATOR_OPERATIONS + "emulator_configuration_setting";
	
	public static final String WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK = WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS + "upload_apk";
	public static final String WEB_PAGE_FOR_OPERATIONS_UPLOAD_APK_WITH_SCRIPT = WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS + "upload_apk_with_script";
	public static final String WEB_PAGE_FOR_OPERATIONS_ESTABLISH_PROJECT = WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS + "establish_project";
	public static final String WEB_PAGE_FOR_OPERATIONS_TERMINATE_PROJECT = WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS + "terminate_project";
	public static final String WEB_PAGE_FOR_OPERATIONS_DELETE_PROJECT = WEB_PAGE_DIR_FOR_PROJECT_OPERATIONS + "delete_project";
	public static final String WEB_PAGE_FOR_ESTABLISHED_PROJECTS_LIST = WEB_PAGE_DIR_FOR_PROJECT_LISTS + "established_projects_list";
	public static final String WEB_PAGE_FOR_UNESTABLISHED_PROJECTS_LIST = WEB_PAGE_DIR_FOR_PROJECT_LISTS + "unestablished_projects_list";
	public static final String WEB_PAGE_FOR_FINISHED_PROJECTS_LIST = WEB_PAGE_DIR_FOR_PROJECT_LISTS + "finished_projects_list";
	public static final String WEB_PAGE_FOR_OPERATION_DONE = WEB_PAGE_DIR_FOR_INFO + "operation_done_info";
	public static final String WEB_PAGE_FOR_TREE_DIAGRAM_RESULT = WEB_PAGE_DIR_FOR_INFO + "tree_diagram_result";
	
	public static final String WEB_PAGE_FOR_GOOGLE_PLAY_CRAWLER_CRAWL_TASK = WEB_PAGE_DIR_FOR_GOOGLE_PLAY_CRAWLER_ROOT + "crawl_apk";

}
