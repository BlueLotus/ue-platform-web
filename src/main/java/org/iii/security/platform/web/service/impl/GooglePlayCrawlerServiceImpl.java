package org.iii.security.platform.web.service.impl;

import java.io.InputStream;

import org.iii.security.platform.core.domain.APKDownloadInfo;
import org.iii.security.platform.web.service.GooglePlayCrawlerService;
import org.iii.security.platform.web.util.crawler.GooglePlayCrawler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class GooglePlayCrawlerServiceImpl implements GooglePlayCrawlerService {
	
	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerServiceImpl.class);

	@Autowired
	private GooglePlayCrawler googlePlayCrawler;
	
	@Override
	public InputStream crawlAPKWithPackageName(String packageName) throws Exception {
		logger.debug("Crawler initialization start...");
		crawlerInitialization();
		logger.debug("Crawler initialization successfully...");
		return downloadAPKFile(packageName);
	}
	
	private void crawlerInitialization() throws Exception {
			googlePlayCrawler.setup();
			googlePlayCrawler.checkin();
			googlePlayCrawler.login();
			googlePlayCrawler.uploadDeviceConfiguration();
	}
	
	private InputStream downloadAPKFile(String packageName) throws Exception {
		APKDownloadInfo apkDownloadInfo = googlePlayCrawler.getDownloadInfo(packageName);
		return googlePlayCrawler.downloadAPKFileWithAPKDownloadInfo(apkDownloadInfo);
	}

}
