package org.iii.security.platform.web.util.constant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformWebRouting {
	
	public static final String WEB_APP_CONTEXT_ROOT = "/ue-platform";
	
	public static final String ROUTING_ROOT_FOR_EMULATOR = WEB_APP_CONTEXT_ROOT + "/emulator";
	public static final String ROUTING_ROOT_FOR_PROJECT = WEB_APP_CONTEXT_ROOT + "/project";
	public static final String ROUTING_ROOT_FOR_ACTIVITY_SNAPSHOT = WEB_APP_CONTEXT_ROOT + "/analysis/activity";
	public static final String ROUTING_ROOT_FOR_GOOGLE_PLAY_CRAWLER = WEB_APP_CONTEXT_ROOT + "/google-play-crawler";
	
	public static final String URL_FOR_HOME_PAGE = WEB_APP_CONTEXT_ROOT + "/home";
	
	public static final String URL_FOR_EMULATOR_DETAIL = "/detail";
	public static final String URL_FOR_ALL_EMULATORS = "/allEmulators";
	public static final String URL_FOR_CREATE_EMULATOR = "/create";
	public static final String URL_FOR_EMULATOR_MANAGEMENT = "/management";
	public static final String URL_FOR_EMULATOR_CONFIG_SETTING = "/setConfiguration";
	public static final String URL_FOR_CREATE_SYSTEM_CONFIG = "/system/create";
	public static final String URL_FOR_INITIALIZE_EMULATOR_STATE = "/init";
	
	public static final String URL_FOR_PROJECT_UPLOAD_MANUAL_TESTING_OPERATION = "/uploadForManualTesting";
	public static final String URL_FOR_PROJECT_UPLOAD_AUTO_TESTING_OPERATION = "/uploadForAutoTesting";
	public static final String URL_FOR_PROJECT_ESTABLISH_CONFIRM = "/establishConfirm";
	public static final String URL_FOR_PROJECT_ESTABLISH_OPERATION ="/establish";
	public static final String URL_FOR_PROJECT_TERMINATE_OPERATION = "/terminate";
	public static final String URL_FOR_PROJECT_DELETE_OPERATION = "/delete";
	public static final String URL_FOR_ESTABLISHED_PROJECTS = "/establishedProjects";
	public static final String URL_FOR_UNESTABLISHED_PROJECTS = "/unestablishedProjects";
	public static final String URL_FOR_FINISHED_PROJECTS = "/finishedProjects";
	public static final String URL_FOR_OPERATION_DONE = "/operationDone";
	public static final String URL_FOR_SHOW_TREE_DIAGRAM = "/showTreeDiagram";
	public static final String URL_FOR_RUN_AUTO_TESTING = "/runAutoTesting";
	
	public static final String URL_FOR_CRAWLER_TASK = "/crawl";

}
