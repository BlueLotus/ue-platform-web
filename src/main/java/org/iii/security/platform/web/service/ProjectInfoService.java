package org.iii.security.platform.web.service;

import java.util.List;

import org.iii.security.platform.core.domain.Project;
import org.iii.security.platform.core.domain.TreeDiagramResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface ProjectInfoService {
	
	Project findById(long id);
	
	boolean establish(Project project);
	
	boolean terminate(Project project);
	
	Project saveProject(Project project);
	
	boolean deleteProject(Project project);
	
	List<String> findUsesPermissionsByProjectId(long projectId);
	
	TreeDiagramResult findTreeDiagramResultByProjectId(long projectId);
	
	Page<Project> getAllEstablishedProjectsDesc(Pageable pageable);
	
	Page<Project> getAllUnestablishedProjectsAsc(Pageable pageable);
	
	Page<Project> getAllFinishedProjectsDesc(Pageable pageable);
	
	void captureCurrentActivitySnapshotByProjectId(long projectId) throws Exception;
	
	void playbackForProject(long projectId) throws Throwable;

}
